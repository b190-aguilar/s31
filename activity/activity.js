// -What directive is used by Node.js in loading the modules it needs?
        /*
            we use the require() directive
        */

// - What Node.js module contains a method for server creation?
        /*
            createServer() 
        */

// - What is the method of the http object responsible for creating a server using Node.js?
        /*
            function(request, response)
        */

// - What method of the response object allows us to set status codes and content types?
        /*
            writeHead()
        */

// - Where will console.log() output its contents when run in Node.js?
        /*
            in the terminal (ex. GitBash)
        */

// - What property of the request object contains the address's endpoint?
        /*
            ".url"
        */