const http = require("http");

// storing the 4000 in a variable port
const port = 4000;

// storing the create server method inside the server variable
const server = http.createServer((request, response) => {
	/*
		"request" is an object that is sent via the client (browser)
		"url" is the propert of the request that referes  to the endpoint of the link
	*/

	// condition below means that we have to access the localhost:4000 with /greeting in its endpoint

	if (request.url === "/greeting"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Hello Again");	
	}


	/*
		create another response with "Hello" with 200 as the status code and plain text as the content type when we try to access /homepage endpoint
	*/

	else if (request.url === "/homepage"){
		response.writeHead(200, {"Content-Type":"text/plain"});
		response.end("Hello");	
	}

	else{
		response.writeHead(404, {"Content-Type":"text/plain"});
		response.end("Page not found");	
	}

});

server.listen(port);


console.log(`Server now running at port ${port}`);