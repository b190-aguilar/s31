/*
	we use "require" directive to load Node.js modules.

	module is a software component or a part of a program that contains one or more routiness

	"http" module lets Node.js transfer data using Hyper text Transfer Protocol.

	"http" module is a set of individual files that contain code to recieve a "component" that helps estabish data transfer between applications

	HTTP is a protocol that allows the fetching of resources such as HTML documents

	Clients (browser) and server (Node.js/expressJS applications) communicate by exchangin individual messages.

	messages that came from the client - REQUESTS

	messages that came from the server - RESPONSE

*/


let http = require("http");

/*
	http - we are now trying to use the http module for us to create our server-side application

	createServer() - found inside the http module; a method that accepts a function as its argument for a creation of a server

	(request, response) - arguments that are passed to the createServer method; this would allow us to receive requests (1st parameter), and send responses (2nd parameter).
*/

// .listen() - allows our application to be run in our local devices through a specifiend port.
/*
	port - virtual point where network connections start and end.
*/

// the code below means that the server will be assigned to port 4000 via listen(4000) method where the server will listen to any requests that are sent to it eventually communicating with our server.

http.createServer(function(request, response) {

	response.writeHead(200, {"Content-Type":"text/plain"});
	// we use writeHead() to:
		// set a status code for the response - a 200 means OK status
		// set Content-Type; using "text/plain" means that we are sending plain text as a response
	response.end("Hello World");
		// we use response.end to denote the last stage of the communication which is the sending of the response from the server
		// to code will send the response "Hello World"

}).listen(4000);


/*
	use node.js to run the server
	press ctrl + c to terminate the gitbash process
*/



// used to confirm is a server is running on a port

console.log("Server running at port: 4000");




/*
	nodemon
		-installing this package will allow the server to automatically restart when files have been changed for updates i.e saving the files.

		SYNTAX
			npm install -g nodemon

		-"npm install" means that we are going to access the npm and install one of the packages thats in its library

		- "-g" means "GLOBAL"

		- "nodemon" - is the package that we are going to install
*/

